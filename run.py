import os , socket
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
# ip = socket.gethostbyname(socket.gethostname())
path = './de'
addr = ('127.0.0.1',21)
authorizer = DummyAuthorizer()
authorizer.add_user('admin','admin','.',perm='elradfmw')

handler = FTPHandler
handler.authorizer = authorizer

server = FTPServer(addr,handler)
server.serve_forever()
